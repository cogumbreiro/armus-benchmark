PROJ = /home/tiago/Work/butter-x10
SOURCE = $(PROJ)/target
JARS = jars
STREAMING_BENCHMARK_DIR = /home/tiago/Work/x10-streaming/fft2
STREAMING_BENCHMARK = $(STREAMING_BENCHMARK_DIR)/StreamingBenchmark.x10
STREAMING = $(JARS)/streaming.jar
STREAMING_INST = $(JARS)/streaming-inst.jar

ARMUSC = $(SOURCE)/armusc-x10.jar
ARMUS_RT = $(JARS)/armus-rt-x10.jar
ARMUS = $(JARS)/armus-x10.jar

all: update-jars $(STREAMING) $(STREAMING_INST) jars/JGFMolDynBench.jar jars/JGFMolDynBench-inst.jar jars/spectralnorm.jar jars/spectralnorm-inst.jar

init:
	(cd $(PROJ); ant)

update-jars: $(JARS)/armus-rt-x10.jar $(JARS)/armus-x10.jar $(STREAMING) $(STREAMING_INST)

$(ARMUS_RT): $(SOURCE)/armus-rt-x10.jar
	cp $(SOURCE)/armus-rt-x10.jar $(JARS)/

$(ARMUS): $(SOURCE)/armus-x10.jar
	cp $(SOURCE)/armus-x10.jar $(JARS)/

jars/%.jar: src/%
	$(eval TMPDIR := $(shell mktemp -d))
	@test -d $(TMPDIR)
	x10c -sourcepath $< -d $(TMPDIR) $(shell find $< -name $(shell basename $<).x10)
	rm $(TMPDIR)/*.java
	jar cf $@ -C $(TMPDIR) .
	rm -f $(TMPDIR)/*.class
	rmdir $(TMPDIR)

jars/%-inst.jar: jars/%.jar
	java -jar $(ARMUSC) $< $@

$(STREAMING): $(STREAMING_BENCHMARK)
	$(eval TMPDIR := $(shell mktemp -d))
	@test -d $(TMPDIR)
	x10c -sourcepath $(STREAMING_BENCHMARK_DIR) $(STREAMING_BENCHMARK) -d $(TMPDIR)
	rm $(TMPDIR)/*.java
	jar cf $(STREAMING) -C $(TMPDIR) .
	rm -f $(TMPDIR)/*.class
	rmdir $(TMPDIR)

#$(STREAMING_INST): $(STREAMING)
#	java -jar $(ARMUSC) $(STREAMING) $(STREAMING_INST)

clean:
	rm -f $(ARMUS) $(ARMUS_RT) $(STREAMING) $(STREAMING_INST)
