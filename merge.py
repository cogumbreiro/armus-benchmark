import csv
from numpy import array

class Data:
    def __init__(self, elapsed):
        self.data = [elapsed]
    def append(self, elapsed):
        self.data.append(elapsed)
        
    def average(self):
        return float(sum(self.data)) / len(self.data)

    def compute(self):
        data = array(self.data)
        return (data.mean(), data.std())
    
    def __repr__(self):
        return repr(compute)

def convert_data(fd):
    for row in csv.reader(fd):
        yield int(row[0]), int(row[1]), int(row[2])
def merge_runs(data):
    result = {}
    
    for (pnum, cnum, elapsed) in data:
        try:
            result[(pnum, cnum)].append(elapsed)
        except KeyError:
            result[(pnum, cnum)] = Data(elapsed)

    return result

def to_row(merged):
    for ((p, c), d) in merged.items():
        mean, std = d.compute()
        yield p, c, mean, std

def cmp_row(row1, row2):
    result = 0
    for (r1, r2) in zip(row1, row2):
        result = cmp(r1, r2)
        if result != 0:
            return result
    return result

def main(filename):
    with open(filename) as fd:
        data = list(to_row(merge_runs(convert_data(fd))))
        data.sort(cmp_row)
        for (p, c, mean, std) in data:
            print ",".join(map(str, (p, c, mean, std)))

if __name__ == '__main__':
    import sys
    main(sys.argv[1])
