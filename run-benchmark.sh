#!/bin/bash
jar=$1
extra=$2

# Shouldn't be changed
cpu_count=$(grep processor /proc/cpuinfo | wc -l)
participants_start=$cpu_count
#participants_end=$cpu_count
let "participants_end=2*$participants_start"
clocks_start=1
clocks_end=16
runs=20

###########################################################

test $participants_start -ge 2 || (echo "participants_start >= 2"; exit 1)
test $participants_end -ge $participants_start || (echo "participants_end >= participants_start"; exit 1)
test $clocks_start -ge 1 || (echo "clocks_start >= 1"; exit 1)
test $clocks_end -ge $clocks_start || (echo "clocks_end >= clocks_start"; exit 1)
test $runs -ge 1 || (echo "runs >= 1"; exit 1)
test -n $jar || (echo "must supply a 'jar'"; exit 1)
for clockId in $(seq $clocks_start $clocks_end); do
	for participants in $(seq $participants_start $participants_end); do
		for r in $(seq 1 $runs); do
			elapsed=$(x10 -classpath $jar $extra StreamingBenchmark $participants $clockId)
			echo $participants,$clockId,$elapsed
		done
    done
done
