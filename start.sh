#!/bin/bash
test -d x10 && export PATH=$PATH:$PWD/x10/bin
which x10 > /dev/null || (echo "x10 not found!"; exit -1)
JAR1=jars/streaming.jar
JAR2=jars/streaming-inst.jar
./run-benchmark.sh $JAR1 >> data/orig.csv
./run-benchmark.sh $JAR2:jars/armus-rt-x10.jar >> data/inst.csv
./run-benchmark.sh $JAR1 -J-javaagent:jars/armus-x10.jar >> data/inj.csv
